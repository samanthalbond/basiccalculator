
//Created by Samantha Bond - 92018224

package calculator;

import java.text.DecimalFormat;

public class Calculations {
    
    //declaring variables
    private double num1 = 0.0d;
    private double num2;
    private double result;
    private char operator;
    private String num1Result;
    private boolean degrees = false;
    
    //get and set methods for variables
    public double getNum1()
    {
        return num1;
    }
    public void setNum1(double num1)
    {
        this.num1 = num1;
    }
    public double getNum2()
    {
        return num2;
    }
    public void setNum2(double num2)
    {
        this.num2 = num2;
    }
    public double getResult()
    {
        return result;
    }
    public void setResult(double result)
    {
        this.result = result;
    }
    public char getOperator()
    {
        return operator;
    }
    public void setOperator(char operator)
    {
        this.operator = operator;
    }
    public String getNum1Result()
    {
        return num1Result;
    }
    public void setNum1Result(String num1Result)
    {
        this.num1Result = num1Result;
    }
    public boolean isDegrees()
    {
        return degrees;
    }
    public void setDegrees(boolean value)
    {
        this.degrees = value;
    }
    
    
    //clear method for use with clear button (CA button)
    public void CA()
    {
        num1 = 0.0;
        num2 = 0.0;
        result = 0.0;
        operator = '\u0000'; 
    }
    
    
    //new decimal formats for use with rounding menu
    DecimalFormat dec0 = new DecimalFormat("#");
    DecimalFormat dec1 = new DecimalFormat("#.#");
    DecimalFormat dec2 = new DecimalFormat("#.##");
    DecimalFormat dec3 = new DecimalFormat("#.###");
    DecimalFormat dec4 = new DecimalFormat("#.####");
    DecimalFormat dec5 = new DecimalFormat("#.#####");
    DecimalFormat dec6 = new DecimalFormat("#.######");
    DecimalFormat dec7 = new DecimalFormat("#.#######");
    DecimalFormat dec8 = new DecimalFormat("#.########");
    DecimalFormat dec9 = new DecimalFormat("#.#########");
    
    
    //method used to round numbers to desired decimal points
    //used with rounding menu
    public String roundNum(int i, double value)
    {
        String roundedValue = "";
        
        switch (i)
        {
            case 0:
                roundedValue = dec0.format(value);
                break;
            case 1:
                roundedValue = dec1.format(value);
                break;
            case 2:
                roundedValue = dec2.format(value);
                break;
            case 3:
                roundedValue = dec3.format(value);
                break;
            case 4:
                roundedValue = dec4.format(value);
                break;
            case 5:
                roundedValue = dec5.format(value);
                break;
            case 6:
                roundedValue = dec6.format(value);
                break;
            case 7:
                roundedValue = dec7.format(value);
                break;
            case 8: 
                roundedValue = dec8.format(value);
                break;
            case 9:
                roundedValue = dec9.format(value);
                break;
        }
        return roundedValue;
    }
    
    
    //method for calculating basic arithmetic operations
    public String calculate()
    {
        switch (operator)
        {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                if (num2 != 0)
                {
                    result = num1 / num2;
                    break;
                }
                else
                {
                    throw new ArithmeticException();
                }
            case 'i':
                result = 1 / num1;
                break;
            case '!':
                result = 1;
                for (int i = 1; i <= num1; i++)
                {
                    result *= i;
                }
                break;
            case '^':
                result = Math.pow(num1, num2);
                break;
        }
        return String.valueOf(result);
    }
    
    //method for calculating results based on multiple operands
    //being entered
    public String multipleOpCalc(String operand, char operator)
    {
        switch (getOperator()) {
            case '\u0000':
                setOperator(operator);
                setNum1(Float.parseFloat(operand));
                break;
            case '!':
            case 'i':
                setOperator(operator);
                setNum1(Double.parseDouble(operand));
                break;
            default:
                setNum2(Double.parseDouble(operand));
                setNum1Result(calculate());
                setNum1(Double.parseDouble(getNum1Result()));
                setOperator(operator);
                break;
        }
        return String.valueOf(result);
    }
    
    //method for calculating results based on a single operand
    public String singleOpCalc(String operand, char operator)
    {
        switch (operator)
        {
            case '!':
                if (Double.parseDouble(operand) >= 0 
                        && Double.parseDouble(operand) <= 20)
                {
                    setOperator(operator);
                    setNum1(Double.parseDouble(operand));
                }
                else
                {
                    throw new NumberFormatException();
                }
                break;
            case 'i':
                if(Double.parseDouble(operand) != 0)
                {
                    setOperator(operator);
                    setNum1(Double.parseDouble(operand));
                }
                else
                {
                    throw new ArithmeticException();
                }
                break;                      
        }
        return String.valueOf(result);
    }
    
    //method to set variables for use with trigonomic calculations
    public void setTrigValues(char operator, String operand)
    {
        setOperator(operator);
        setNum1(Double.parseDouble(operand));
    }
    
    //method that performs the trigonomic calculations based on whether degrees
    //or radians are selected, and whether sin, cos & tan, or asin, acos & atan
    //are selected
    public String trigCalculations(char operator)
    {
        if (isDegrees() && operator == 'S' || operator == 'T' || operator == 'C')
        {
            num1 = Math.toRadians(num1);
        }
        else if (isDegrees() && operator == 's' || operator == 't' || operator == 'c')
        {
            switch (operator)
            {
                case 's':
                    result = Math.asin(num1);
                    break;
                case 't':
                    result = Math.atan(num1);
                    break;
                case 'c':
                    result = Math.acos(num1);
                    break;
            }
            result = Math.toDegrees(result);  
        }
        else if (operator == 's' || operator == 't' || operator == 'c')
        {
            switch (operator)
            {
                case 's':
                    result = Math.asin(num1);
                    break;
                case 't':
                    result = Math.atan(num1);
                    break;
                case 'c':
                    result = Math.acos(num1);
                    break;
            }
        }
        switch (operator)
        {
            case 'S':
                result = Math.sin(num1);
                break;
            case 'T':
                result = Math.tan(num1);
                break;
            case 'C':
                result = Math.cos(num1);            
        }
        return String.valueOf(result);
    }  
}
