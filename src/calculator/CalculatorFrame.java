
//Created by Samantha Bond - 92018224

package calculator;

import java.awt.Font;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;

public class CalculatorFrame extends javax.swing.JFrame {

    public CalculatorFrame() {
        initComponents();
        setSize(290,415);
        panExtended.setVisible(false);
        calc = new Calculations();
    }
    
    //setting constructor for calculations class
    Calculations calc;
    
    //declaring variables
    private String equals = "";
    private double number = 0;
    private String operand = "";
    

    //method used with action events to determine which number has been selected
    public void selectedNum(String num)
    {
        if ("selected".equals(equals))
        {
            equals = "";
            txtInput.setText(num);
            operand = num;
        }
        else
        {
            operand += num;
            txtInput.setText(operand);
        }
    }
    
    //method used to round numbers based on rounding menu selection
    public void roundingNum(JRadioButtonMenuItem rad, int decValue)
    {
        if (rad.isSelected() && !txtInput.equals(""))
        {
            number = Double.parseDouble(txtInput.getText());
            txtInput.setText(calc.roundNum(decValue, number)); 
        }
    }
    
    //method used to clear calculator
    public void CA()
    {
        txtInput.setText("");
        operand = "";
        calc.CA();
    }
    
    //method that handles button selections to perform calculations on multiple
    //operands  -   also catches exceptions and displays error to user
    public void multipleOpCalcSelected(char operator)
    {
        try
        {
            if (operator == '\u0000')
            {
                calc.multipleOpCalc(operand, operator);
                txtInput.setText("");
                operand = "";
            }
            else if (calc.getOperator() == '!' || calc.getOperator() == 'i')
            {
                operand = txtInput.getText();
                calc.multipleOpCalc(operand, operator);
                txtInput.setText("");
                operand = "";
            }
            else
            {
                calc.multipleOpCalc(operand, operator);
                txtInput.setText(calc.getNum1Result());
                operand = "";
            }
        }
        catch (NumberFormatException nfe)
        {
            msgError.showMessageDialog(this, "Please enter a postive integer between"
                    + " 0 and 20", "Number Format Error", JOptionPane.INFORMATION_MESSAGE);
        }
        catch (ArithmeticException ae)
        {
            msgError.showMessageDialog(this, "Not possible to divide by zero", 
                    "Arithmetic Error", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    //method that handles button selections to perform calculations on single
    //operands  -   also catches exceptions and displays error to user
    public void singleOpCalcSelected(char operator)
    {
        try
        {
            if (operator == '!')
            {
                operand = String.valueOf(Integer.parseInt(operand));
            }
            calc.singleOpCalc(operand, operator);
            txtInput.setText(calc.calculate());
            operand = String.valueOf(Double.parseDouble(calc.calculate()));
        }
        catch (NumberFormatException nfe)
        {
            msgError.showMessageDialog(this, "Please enter a postive integer between"
                    + " 0 and 20", "Number Format Error", JOptionPane.INFORMATION_MESSAGE);
        }
        catch (ArithmeticException ae)
        {
            msgError.showMessageDialog(this, "Not possible to divide by zero", 
                    "Arithmetic Error", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    //method to calculate and display value of PI
    public void piCalc(char operator)
    {
        txtInput.setText(String.valueOf(Math.PI));
        calc.setNum1(Math.PI);
        operand = String.valueOf(Math.PI);
    }
    
    //method to calculate and display value of E
    public void eCalc(char operator) 
    {
        txtInput.setText(String.valueOf(Math.E));
        calc.setNum1(Math.PI);
        operand = String.valueOf(Math.E); 
    }
    
    //method to handle button selections and perform trigonomic calculations
    public void trigCalculations(char operator)
    {
        calc.setTrigValues(operator, operand);
        txtInput.setText((calc.trigCalculations(operator)));
        operand = txtInput.getText();
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgrDegreeRadius = new javax.swing.ButtonGroup();
        bgrView = new javax.swing.ButtonGroup();
        bgrRound = new javax.swing.ButtonGroup();
        bgrFont = new javax.swing.ButtonGroup();
        msgAbout = new javax.swing.JOptionPane();
        msgHotKeys = new javax.swing.JOptionPane();
        msgError = new javax.swing.JOptionPane();
        txtInput = new javax.swing.JTextField();
        btn1 = new javax.swing.JButton();
        btn2 = new javax.swing.JButton();
        btn3 = new javax.swing.JButton();
        btnMultiply = new javax.swing.JButton();
        btnInverse = new javax.swing.JButton();
        btn4 = new javax.swing.JButton();
        btn7 = new javax.swing.JButton();
        btn0 = new javax.swing.JButton();
        btn5 = new javax.swing.JButton();
        btn6 = new javax.swing.JButton();
        btn8 = new javax.swing.JButton();
        btnDivide = new javax.swing.JButton();
        btnPow = new javax.swing.JButton();
        btn9 = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnNegate = new javax.swing.JButton();
        btnDec = new javax.swing.JButton();
        btnEquals = new javax.swing.JButton();
        btnSubtract = new javax.swing.JButton();
        btnFactorial = new javax.swing.JButton();
        panExtended = new javax.swing.JPanel();
        btnSin = new javax.swing.JButton();
        btnCos = new javax.swing.JButton();
        btnTan = new javax.swing.JButton();
        btnPI = new javax.swing.JButton();
        btnAsin = new javax.swing.JButton();
        btnAcos = new javax.swing.JButton();
        btnAtan = new javax.swing.JButton();
        btnE = new javax.swing.JButton();
        radRadius = new javax.swing.JRadioButton();
        radDegrees = new javax.swing.JRadioButton();
        btnCA = new javax.swing.JButton();
        btnCE = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        mnuView = new javax.swing.JMenu();
        mnuBasic = new javax.swing.JRadioButtonMenuItem();
        mnuExtended = new javax.swing.JRadioButtonMenuItem();
        mnuFont = new javax.swing.JMenu();
        mnuPlain = new javax.swing.JRadioButtonMenuItem();
        mnuBold = new javax.swing.JRadioButtonMenuItem();
        mnuRound = new javax.swing.JMenu();
        rad0 = new javax.swing.JRadioButtonMenuItem();
        rad1 = new javax.swing.JRadioButtonMenuItem();
        rad2 = new javax.swing.JRadioButtonMenuItem();
        rad3 = new javax.swing.JRadioButtonMenuItem();
        rad4 = new javax.swing.JRadioButtonMenuItem();
        rad5 = new javax.swing.JRadioButtonMenuItem();
        rad6 = new javax.swing.JRadioButtonMenuItem();
        rad7 = new javax.swing.JRadioButtonMenuItem();
        rad8 = new javax.swing.JRadioButtonMenuItem();
        rad9 = new javax.swing.JRadioButtonMenuItem();
        mnuHelp = new javax.swing.JMenu();
        mnuAbout = new javax.swing.JMenuItem();
        mnuHotKeys = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Calculator");
        setResizable(false);

        txtInput.setEditable(false);

        btn1.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn1.setText("1");
        btn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1ActionPerformed(evt);
            }
        });

        btn2.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn2.setText("2");
        btn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2ActionPerformed(evt);
            }
        });

        btn3.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn3.setText("3");
        btn3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3ActionPerformed(evt);
            }
        });

        btnMultiply.setText("*");
        btnMultiply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMultiplyActionPerformed(evt);
            }
        });

        btnInverse.setText("1/x");
        btnInverse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInverseActionPerformed(evt);
            }
        });

        btn4.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn4.setText("4");
        btn4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4ActionPerformed(evt);
            }
        });

        btn7.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn7.setText("7");
        btn7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7ActionPerformed(evt);
            }
        });

        btn0.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn0.setText("0");
        btn0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn0ActionPerformed(evt);
            }
        });

        btn5.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn5.setText("5");
        btn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5ActionPerformed(evt);
            }
        });

        btn6.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn6.setText("6");
        btn6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6ActionPerformed(evt);
            }
        });

        btn8.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn8.setText("8");
        btn8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8ActionPerformed(evt);
            }
        });

        btnDivide.setText("/");
        btnDivide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDivideActionPerformed(evt);
            }
        });

        btnPow.setText("x^y");
        btnPow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPowActionPerformed(evt);
            }
        });

        btn9.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btn9.setText("9");
        btn9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn9ActionPerformed(evt);
            }
        });

        btnAdd.setText("+");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnNegate.setText("+/-");
        btnNegate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNegateActionPerformed(evt);
            }
        });

        btnDec.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btnDec.setText(".");
        btnDec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDecActionPerformed(evt);
            }
        });

        btnEquals.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        btnEquals.setText("=");
        btnEquals.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEqualsActionPerformed(evt);
            }
        });

        btnSubtract.setText("-");
        btnSubtract.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubtractActionPerformed(evt);
            }
        });

        btnFactorial.setText("x!");
        btnFactorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFactorialActionPerformed(evt);
            }
        });

        btnSin.setText("sin");
        btnSin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSinActionPerformed(evt);
            }
        });

        btnCos.setText("cos");
        btnCos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCosActionPerformed(evt);
            }
        });

        btnTan.setText("tan");
        btnTan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTanActionPerformed(evt);
            }
        });

        btnPI.setText("PI");
        btnPI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPIActionPerformed(evt);
            }
        });

        btnAsin.setText("asin");
        btnAsin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAsinActionPerformed(evt);
            }
        });

        btnAcos.setText("acos");
        btnAcos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcosActionPerformed(evt);
            }
        });

        btnAtan.setText("atan");
        btnAtan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtanActionPerformed(evt);
            }
        });

        btnE.setText("E");
        btnE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEActionPerformed(evt);
            }
        });

        bgrDegreeRadius.add(radRadius);
        radRadius.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        radRadius.setSelected(true);
        radRadius.setText("Radius");
        radRadius.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radRadiusItemStateChanged(evt);
            }
        });
        radRadius.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radRadiusActionPerformed(evt);
            }
        });

        bgrDegreeRadius.add(radDegrees);
        radDegrees.setFont(new java.awt.Font("Lucida Grande", 0, 12)); // NOI18N
        radDegrees.setText("Degrees");
        radDegrees.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radDegreesItemStateChanged(evt);
            }
        });
        radDegrees.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radDegreesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panExtendedLayout = new javax.swing.GroupLayout(panExtended);
        panExtended.setLayout(panExtendedLayout);
        panExtendedLayout.setHorizontalGroup(
            panExtendedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panExtendedLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panExtendedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(radRadius)
                    .addGroup(panExtendedLayout.createSequentialGroup()
                        .addComponent(btnSin, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAsin, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panExtendedLayout.createSequentialGroup()
                        .addComponent(btnCos, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAcos, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panExtendedLayout.createSequentialGroup()
                        .addComponent(btnTan, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAtan, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panExtendedLayout.createSequentialGroup()
                        .addComponent(btnPI, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnE, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(radDegrees))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panExtendedLayout.setVerticalGroup(
            panExtendedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panExtendedLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(radRadius)
                .addGap(3, 3, 3)
                .addComponent(radDegrees)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panExtendedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAsin, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSin, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panExtendedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCos, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAcos, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panExtendedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTan, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAtan, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panExtendedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPI, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnE, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        btnCA.setText("CA");
        btnCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCAActionPerformed(evt);
            }
        });

        btnCE.setText("CE");
        btnCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCEActionPerformed(evt);
            }
        });

        mnuView.setText("View");

        mnuBasic.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, 0));
        bgrView.add(mnuBasic);
        mnuBasic.setMnemonic('B');
        mnuBasic.setSelected(true);
        mnuBasic.setText("Basic");
        mnuBasic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuBasicActionPerformed(evt);
            }
        });
        mnuView.add(mnuBasic);

        mnuExtended.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, 0));
        bgrView.add(mnuExtended);
        mnuExtended.setMnemonic('E');
        mnuExtended.setText("Extended");
        mnuExtended.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuExtendedActionPerformed(evt);
            }
        });
        mnuView.add(mnuExtended);

        jMenuBar1.add(mnuView);

        mnuFont.setText("Font");

        mnuPlain.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        bgrFont.add(mnuPlain);
        mnuPlain.setSelected(true);
        mnuPlain.setText("Plain");
        mnuPlain.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuPlainActionPerformed(evt);
            }
        });
        mnuFont.add(mnuPlain);

        mnuBold.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_MASK));
        bgrFont.add(mnuBold);
        mnuBold.setText("Bold");
        mnuBold.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuBoldActionPerformed(evt);
            }
        });
        mnuFont.add(mnuBold);

        jMenuBar1.add(mnuFont);

        mnuRound.setText("Round");

        bgrRound.add(rad0);
        rad0.setSelected(true);
        rad0.setText("0");
        rad0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad0ActionPerformed(evt);
            }
        });
        mnuRound.add(rad0);

        bgrRound.add(rad1);
        rad1.setText("1");
        rad1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad1ActionPerformed(evt);
            }
        });
        mnuRound.add(rad1);

        bgrRound.add(rad2);
        rad2.setText("2");
        rad2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad2ActionPerformed(evt);
            }
        });
        mnuRound.add(rad2);

        bgrRound.add(rad3);
        rad3.setText("3");
        rad3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad3ActionPerformed(evt);
            }
        });
        mnuRound.add(rad3);

        bgrRound.add(rad4);
        rad4.setText("4");
        rad4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad4ActionPerformed(evt);
            }
        });
        mnuRound.add(rad4);

        bgrRound.add(rad5);
        rad5.setText("5");
        rad5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad5ActionPerformed(evt);
            }
        });
        mnuRound.add(rad5);

        bgrRound.add(rad6);
        rad6.setText("6");
        rad6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad6ActionPerformed(evt);
            }
        });
        mnuRound.add(rad6);

        bgrRound.add(rad7);
        rad7.setText("7");
        rad7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad7ActionPerformed(evt);
            }
        });
        mnuRound.add(rad7);

        bgrRound.add(rad8);
        rad8.setText("8");
        rad8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad8ActionPerformed(evt);
            }
        });
        mnuRound.add(rad8);

        bgrRound.add(rad9);
        rad9.setText("9");
        rad9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rad9ActionPerformed(evt);
            }
        });
        mnuRound.add(rad9);

        jMenuBar1.add(mnuRound);

        mnuHelp.setText("Help");

        mnuAbout.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, 0));
        mnuAbout.setText("About");
        mnuAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuAboutActionPerformed(evt);
            }
        });
        mnuHelp.add(mnuAbout);

        mnuHotKeys.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, 0));
        mnuHotKeys.setText("Hot Keys");
        mnuHotKeys.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuHotKeysActionPerformed(evt);
            }
        });
        mnuHelp.add(mnuHotKeys);

        jMenuBar1.add(mnuHelp);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btn1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btn4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btn0, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDec, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btn7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnCA, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnDivide, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(btnMultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnInverse, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnPow, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(btnCE, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(btn9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(btnEquals, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(btnSubtract, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnFactorial, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnNegate, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addComponent(txtInput, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panExtended, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(184, 184, 184))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtInput, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnMultiply, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnInverse, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn4, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnDivide, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnPow, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn9, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnNegate, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn0, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnDec, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnEquals, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnSubtract, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnFactorial, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCA, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCE, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(panExtended, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mnuPlainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuPlainActionPerformed
        if (mnuPlain.isSelected())
        {
            Font oldFont = txtInput.getFont();
            Font newFont = new Font(oldFont.getName(),Font.PLAIN,oldFont.getSize());
            txtInput.setFont(newFont);
        }
    }//GEN-LAST:event_mnuPlainActionPerformed

    private void mnuBasicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuBasicActionPerformed
        panExtended.setVisible(false);
        setSize(290,415);
    }//GEN-LAST:event_mnuBasicActionPerformed

    private void mnuExtendedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuExtendedActionPerformed
        panExtended.setVisible(true);
        setSize(430,415);
    }//GEN-LAST:event_mnuExtendedActionPerformed

    private void mnuBoldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuBoldActionPerformed
        if (mnuBold.isSelected())
        {
            Font oldFont = txtInput.getFont();
            Font newFont = new Font(oldFont.getName(),Font.BOLD,oldFont.getSize());
            txtInput.setFont(newFont);
        }
    }//GEN-LAST:event_mnuBoldActionPerformed

    private void mnuAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuAboutActionPerformed
        msgAbout.showMessageDialog(this, "Calculator\r\nCreated by: Samantha Bond\r\n26/09/2017", 
                "About", javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_mnuAboutActionPerformed

    private void mnuHotKeysActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuHotKeysActionPerformed
        msgHotKeys.showMessageDialog(this, "A - About\r\nK - Hot Keys\r\nCtrl+P "
                + "- Plain Font\r\nCtrl+B - Bold Font\r\nB - Basic View\r\nE - "
                + "Extended View\r\nAlt+P - PI\r\nAlt+E - E", "Hot Keys", 
                javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_mnuHotKeysActionPerformed

    private void rad0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad0ActionPerformed
        roundingNum(rad0, 0);
    }//GEN-LAST:event_rad0ActionPerformed

    private void rad1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad1ActionPerformed
        roundingNum(rad1, 1);
    }//GEN-LAST:event_rad1ActionPerformed

    private void rad2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad2ActionPerformed
        roundingNum(rad2, 2);
    }//GEN-LAST:event_rad2ActionPerformed

    private void rad3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad3ActionPerformed
        roundingNum(rad3, 3);
    }//GEN-LAST:event_rad3ActionPerformed

    private void rad4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad4ActionPerformed
        roundingNum(rad4, 4);
    }//GEN-LAST:event_rad4ActionPerformed

    private void rad5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad5ActionPerformed
        roundingNum(rad5, 5);
    }//GEN-LAST:event_rad5ActionPerformed

    private void rad6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad6ActionPerformed
        roundingNum(rad6, 6);
    }//GEN-LAST:event_rad6ActionPerformed

    private void rad7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad7ActionPerformed
        roundingNum(rad7, 7);
    }//GEN-LAST:event_rad7ActionPerformed

    private void rad8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad8ActionPerformed
        roundingNum(rad8, 8);
    }//GEN-LAST:event_rad8ActionPerformed

    private void rad9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rad9ActionPerformed
        roundingNum(rad9, 9);
    }//GEN-LAST:event_rad9ActionPerformed

    private void btn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1ActionPerformed
        selectedNum("1");
    }//GEN-LAST:event_btn1ActionPerformed

    private void btn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2ActionPerformed
        selectedNum("2");
    }//GEN-LAST:event_btn2ActionPerformed

    private void btn3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3ActionPerformed
        selectedNum("3");
    }//GEN-LAST:event_btn3ActionPerformed

    private void btn4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4ActionPerformed
        selectedNum("4");
    }//GEN-LAST:event_btn4ActionPerformed

    private void btn5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5ActionPerformed
        selectedNum("5");
    }//GEN-LAST:event_btn5ActionPerformed

    private void btn6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6ActionPerformed
        selectedNum("6");
    }//GEN-LAST:event_btn6ActionPerformed

    private void btn7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7ActionPerformed
        selectedNum("7");
    }//GEN-LAST:event_btn7ActionPerformed

    private void btn8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8ActionPerformed
        selectedNum("8");
    }//GEN-LAST:event_btn8ActionPerformed

    private void btn9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn9ActionPerformed
        selectedNum("9");
    }//GEN-LAST:event_btn9ActionPerformed

    private void btn0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn0ActionPerformed
        selectedNum("0");
    }//GEN-LAST:event_btn0ActionPerformed

    private void btnMultiplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMultiplyActionPerformed
        multipleOpCalcSelected('*');
    }//GEN-LAST:event_btnMultiplyActionPerformed

    private void btnDivideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDivideActionPerformed
        multipleOpCalcSelected('/');
    }//GEN-LAST:event_btnDivideActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        multipleOpCalcSelected('+');
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnSubtractActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubtractActionPerformed
        multipleOpCalcSelected('-');
    }//GEN-LAST:event_btnSubtractActionPerformed

    private void btnEqualsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEqualsActionPerformed
        try
        {
            if (calc.getNum1() == 0.0d)
            {
                txtInput.setText(operand);
            }
            else
            {
                calc.setNum2(Double.parseDouble(txtInput.getText()));
                txtInput.setText(calc.calculate());
                operand = txtInput.getText();
                calc.CA();
            }
            equals = "selected";
        }
        catch (NumberFormatException nfe)
        {
            msgError.showMessageDialog(this, "Please enter a positive integer", 
                    "Number Format Error", JOptionPane.INFORMATION_MESSAGE);
        }
        catch (ArithmeticException ae)
        {
            msgError.showMessageDialog(this, "Division by zero is not possible", 
                    "Arithmetic Error", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnEqualsActionPerformed

    private void btnCAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCAActionPerformed
        CA();
    }//GEN-LAST:event_btnCAActionPerformed

    private void btnCEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCEActionPerformed
        txtInput.setText("");
        operand = "";
    }//GEN-LAST:event_btnCEActionPerformed

    private void btnDecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDecActionPerformed
        if (!txtInput.getText().contains("."))
        {
            operand += ".";
            txtInput.setText(operand);
        }
        else
        {
            msgError.showMessageDialog(this, "No more than one decimal point "
                    + "is allowed", "Decimal Error Message", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnDecActionPerformed

    private void btnInverseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInverseActionPerformed
        singleOpCalcSelected('i');
    }//GEN-LAST:event_btnInverseActionPerformed

    private void btnFactorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFactorialActionPerformed
        singleOpCalcSelected('!');
    }//GEN-LAST:event_btnFactorialActionPerformed

    private void btnPowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPowActionPerformed
        multipleOpCalcSelected('^');
    }//GEN-LAST:event_btnPowActionPerformed

    private void btnNegateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNegateActionPerformed
        if (Double.parseDouble(txtInput.getText()) > 0)
        {
            txtInput.setText(String.valueOf(-(Double.parseDouble(operand))));
            operand = txtInput.getText();
        }
        else
        {
            txtInput.setText(String.valueOf(Math.abs(Double.parseDouble(operand))));
            operand = txtInput.getText();
        }
    }//GEN-LAST:event_btnNegateActionPerformed

    private void btnPIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPIActionPerformed
        piCalc('P');
    }//GEN-LAST:event_btnPIActionPerformed

    private void btnEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEActionPerformed
        eCalc('E');
    }//GEN-LAST:event_btnEActionPerformed

    private void radRadiusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radRadiusActionPerformed
        
    }//GEN-LAST:event_radRadiusActionPerformed

    private void radDegreesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radDegreesActionPerformed
        
    }//GEN-LAST:event_radDegreesActionPerformed

    private void btnSinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSinActionPerformed
        trigCalculations('S');
    }//GEN-LAST:event_btnSinActionPerformed

    private void btnCosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCosActionPerformed
        trigCalculations('C');
    }//GEN-LAST:event_btnCosActionPerformed

    private void btnTanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTanActionPerformed
        trigCalculations('T');
    }//GEN-LAST:event_btnTanActionPerformed

    private void btnAsinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAsinActionPerformed
        trigCalculations('s');
    }//GEN-LAST:event_btnAsinActionPerformed

    private void btnAcosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcosActionPerformed
        trigCalculations('c');
    }//GEN-LAST:event_btnAcosActionPerformed

    private void btnAtanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtanActionPerformed
        trigCalculations('t');
    }//GEN-LAST:event_btnAtanActionPerformed

    private void radRadiusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radRadiusItemStateChanged
        if (radRadius.isSelected())
        {
            calc.setDegrees(false);
        }
    }//GEN-LAST:event_radRadiusItemStateChanged

    private void radDegreesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radDegreesItemStateChanged
        if (radDegrees.isSelected())
        {
            calc.setDegrees(true);
        }
    }//GEN-LAST:event_radDegreesItemStateChanged

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CalculatorFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CalculatorFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CalculatorFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CalculatorFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CalculatorFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgrDegreeRadius;
    private javax.swing.ButtonGroup bgrFont;
    private javax.swing.ButtonGroup bgrRound;
    private javax.swing.ButtonGroup bgrView;
    private javax.swing.JButton btn0;
    private javax.swing.JButton btn1;
    private javax.swing.JButton btn2;
    private javax.swing.JButton btn3;
    private javax.swing.JButton btn4;
    private javax.swing.JButton btn5;
    private javax.swing.JButton btn6;
    private javax.swing.JButton btn7;
    private javax.swing.JButton btn8;
    private javax.swing.JButton btn9;
    private javax.swing.JButton btnAcos;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAsin;
    private javax.swing.JButton btnAtan;
    private javax.swing.JButton btnCA;
    private javax.swing.JButton btnCE;
    private javax.swing.JButton btnCos;
    private javax.swing.JButton btnDec;
    private javax.swing.JButton btnDivide;
    private javax.swing.JButton btnE;
    private javax.swing.JButton btnEquals;
    private javax.swing.JButton btnFactorial;
    private javax.swing.JButton btnInverse;
    private javax.swing.JButton btnMultiply;
    private javax.swing.JButton btnNegate;
    private javax.swing.JButton btnPI;
    private javax.swing.JButton btnPow;
    private javax.swing.JButton btnSin;
    private javax.swing.JButton btnSubtract;
    private javax.swing.JButton btnTan;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem mnuAbout;
    private javax.swing.JRadioButtonMenuItem mnuBasic;
    private javax.swing.JRadioButtonMenuItem mnuBold;
    private javax.swing.JRadioButtonMenuItem mnuExtended;
    private javax.swing.JMenu mnuFont;
    private javax.swing.JMenu mnuHelp;
    private javax.swing.JMenuItem mnuHotKeys;
    private javax.swing.JRadioButtonMenuItem mnuPlain;
    private javax.swing.JMenu mnuRound;
    private javax.swing.JMenu mnuView;
    private javax.swing.JOptionPane msgAbout;
    private javax.swing.JOptionPane msgError;
    private javax.swing.JOptionPane msgHotKeys;
    private javax.swing.JPanel panExtended;
    private javax.swing.JRadioButtonMenuItem rad0;
    private javax.swing.JRadioButtonMenuItem rad1;
    private javax.swing.JRadioButtonMenuItem rad2;
    private javax.swing.JRadioButtonMenuItem rad3;
    private javax.swing.JRadioButtonMenuItem rad4;
    private javax.swing.JRadioButtonMenuItem rad5;
    private javax.swing.JRadioButtonMenuItem rad6;
    private javax.swing.JRadioButtonMenuItem rad7;
    private javax.swing.JRadioButtonMenuItem rad8;
    private javax.swing.JRadioButtonMenuItem rad9;
    private javax.swing.JRadioButton radDegrees;
    private javax.swing.JRadioButton radRadius;
    private javax.swing.JTextField txtInput;
    // End of variables declaration//GEN-END:variables
}
